import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as ss
import os, sys
import shutil
import math as m

row = ['A', 'B', 'C','D','E','F','G','H','I','J','K','L','M','N','O','P']
col = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24']
csvfiles = ['Cal Orange 560', 'FAM', 'Quasar 670']
target_names = ['RP', 'N1', 'N2']

MAX_VALID_KNEE = [32.99, 35.99, 35.99]
MIN_VALID_KNEE = [5, 5, 5]
RFU_MIN_MAX_DELTA = [150, 175, 225]
SLOPE_NORM_TRGT_AT_KNEE = [4.25, 3.25, 4.0]
MAX_KNEEVAL_PCT = 70 # discard if knee val > pct*range

def butter_lowpass_filter(data, a, b):
    nyq = 0.5 * fs  # Nyquist Frequency
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    y = ss.filtfilt(b, a, data)
    return y

def get_butter_lowpass_filter(cutoff, fs, order):
    nyq = 0.5 * fs  # Nyquist Frequency
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = ss.butter(order, normal_cutoff, btype='low', analog=False)
    return [a, b]


def get_slope(data):
    n = np.size(data)
    s = np.zeros(n)
    
    s[0] = data[1] - data[0]
    for row_idx in range(1,n):
        s[row_idx] = data[row_idx] - data[row_idx-1]
    
    return s

def compute_knee(row_idx, col_idx, target_idx, pcrd_csv_path, pcrd_fname, save_plots_flag):
                                      
        
    csvfilename = pcrd_csv_path + '/' + pcrd_fname + ' -  Quantification Amplification Results_' + csvfiles[target_idx]
    #csvfilename = pcrd_csv_path + '/' + pcrd_fname + '_All Wells -  Quantification Amplification Results_' + csvfiles[target_idx] (batch export file name is different)
    #print(csvfilename)
    
    df= pd.read_csv(csvfilename + '.csv')

    cell_name = row[row_idx]+col[col_idx]
    fam_ij = df[cell_name] 
    rfu = fam_ij - min(fam_ij)
    
    rfu_filt =  butter_lowpass_filter(rfu, a_butter, b_butter)
    
    rfu_slope = get_slope(rfu_filt)
    rfu_slope_filt =  butter_lowpass_filter(rfu_slope, a_butter2, b_butter2)
    rfu_slope2 = get_slope(rfu_slope_filt) # second derivative of rfu
    

    RFU_range = max(rfu_filt) - min(rfu_filt)
    rfu_slope2_thresh = max(2.25, RFU_range/175.0)
                    
    range_thresh = max(min(RFU_range/25, 50), 5)
    #print(rfu_slope2_thresh)
    knee = np.argmax(rfu_slope2)
    rfu_at_knee = rfu_filt[knee]
    rfu_slope2_at_knee = rfu_slope2[knee]
    #print('Knee = ' + str(knee))
    
    # check that slope is increasing by a min threshold at 
    # knee. don't think this is needd as this is a weak 
    # condition as it stands now
    idx1 = max(knee-2, 0)
    idx2 = min(knee+2, len(rfu_filt)-1)
    if ((knee-idx1 == 0) or (idx2-knee == 0)): 
        knee = 0
    else:
        knee_val_del1 = (-rfu_filt[idx1] + rfu_filt[knee])/(knee-idx1)
        knee_val_del2 = (rfu_filt[idx2] - rfu_filt[knee])/(idx2-knee)

        if (knee_val_del2 < knee_val_del1*1.25): 
            knee = 0                  
            
    
    # make sure that it is a valid maxima by checking the values 
    # around the maxima by +/-4 indices
    i0 = max(0, knee - 4)
    i1 = min(39, knee + 4)
    for ii in range(knee+1, i1+1):
        if rfu_slope2[ii] > rfu_slope2[ii-1]:
            knee = 0 # force to make it NA
            break
        
    for ii in range(i0, knee-1):
        if rfu_slope2[ii] < rfu_slope2[ii-1]:
            knee = 0 # force to make it NA
            break

    # now pick the knee value that is close to the knee 
    # value picked above based on max slope2, and at 
    # exactly the desired slope using interpolation
    slope_target = SLOPE_NORM_TRGT_AT_KNEE[target_idx]
    rfu_slope_filt_norm = 100*rfu_slope_filt/RFU_range;        
    knee_pt_slope_val = slope_target
    if (knee > 0):
        for knee_idx in range(knee+1, 0, -1):
            if rfu_slope_filt_norm[knee_idx] <= slope_target:

                # line interpolation to get knee value at target slope
                knee2 = knee_idx+1
                knee1 = knee_idx

                if (knee2 >= 39): 
                    knee2 = 39
                    knee1 = 38
                
                rfu_filt_slope_2 = rfu_slope_filt_norm[knee2]
                rfu_filt_slope_1 = rfu_slope_filt_norm[knee1]
                
                slope = (rfu_filt_slope_2-rfu_filt_slope_1)/(knee2-knee1)
                knee = knee2 - (rfu_filt_slope_2-slope_target)/slope
                knee = round(100*knee)/100
                
                knee_pt_slope_val = slope_target
                break
    
    # find the rfu value at the interpolated 
    # knee (used only for plotting)
    knee1 = m.floor(knee)    
    knee2 = knee1 + 1
    if (knee1 >= 39) or (knee1 < 0): 
        knee = 0
        knee_pt_val = 0
    else:    
        rfu_filt_slope_2 = rfu_filt[knee2]
        rfu_filt_slope_1 = rfu_filt[knee1]
        slope = (rfu_filt_slope_2-rfu_filt_slope_1)
        knee_pt_val = rfu_filt_slope_2 - slope*(knee2-knee)
    
    # add one for 0 idx
    knee = knee + 1 
    knee = round(100*knee)/100

    '''        
    print("RFU_range = " + str(RFU_range))
    print("rfu_slope2_at_knee = " + str(rfu_slope2_at_knee))
    print("rfu_slope2_thresh = " + str(rfu_slope2_thresh))
    print('Knee = ' + str(knee))
    '''
    
    if (knee < MIN_VALID_KNEE[target_idx]):
        knee_string='NaN'
    elif (knee > MAX_VALID_KNEE[target_idx]):
        knee_string='NaN'
    elif (RFU_range < RFU_MIN_MAX_DELTA[target_idx]):
        knee_string='NaN'      
    elif (rfu_at_knee > MAX_KNEEVAL_PCT *RFU_range/100):
        knee_string='NaN'                    
    elif (rfu_slope2_at_knee < rfu_slope2_thresh):
        knee_string='NaN'
    elif (knee_val_del2 < range_thresh): 
        knee_string = 'NaN'
    elif (knee_val_del2 < knee_val_del1*1.25): 
        knee_string = 'NaN'                    
    elif (knee >= 40): # high knee not discared here - yet 
        knee_string = 'NaN'
    else:        
        knee_string = str(knee)
    
    if (save_plots_flag==1):    
        # for plotting starting from 1 instead of zero
        cycles = np.arange(1, len(rfu_filt)+1, 1, dtype=int)

        plt.subplot(3,1,1)
        plt.plot(cycles, rfu, marker='.', color='g')
        plt.plot(cycles, rfu_filt, marker='.', color='b')
        plt.xlabel(pcrd_fname)
    
        plt.title('Cell = ' + cell_name + ', ' + 'Target=' + target_names[target_idx] + ', Knee=' + knee_string)
            
        plt.grid()
        plt.ylabel('delta RFU')
     
        if (knee_string != 'NaN'):
            knee_pt = np.array([knee])
            plt.plot(knee_pt, knee_pt_val, marker='X', color='r')
        
        plt.subplot(3,1,2)                
        plt.plot(cycles, rfu_slope, marker='.', color='g')
        plt.grid()
        plt.ylabel('derivative')
        
        
        plt.subplot(3,1,3)                
        plt.plot(cycles, rfu_slope2, marker='.')
        plt.grid()
        plt.xlabel(pcrd_fname)
        plt.ylabel('2nd derivative')
                      
        fig_path = pcrd_csv_path + '/' + pcrd_fname + ' - plots/' + 'Cell-' + cell_name + ' - ' + target_names[target_idx] + ' - Knee-' + knee_string + '.jpg'
        plt.savefig(fig_path, dpi=200)
    
        plt.clf()
    
    return knee_string


fs = 1.0
order = 2
cutoff = 0.1
[a_butter, b_butter] = get_butter_lowpass_filter(cutoff, fs, order)

cutoff = 0.2
[a_butter2, b_butter2] = get_butter_lowpass_filter(cutoff, fs, order)

def main(pcrd_csv_path, pcrd_fname, save_plots_flag):
        
    #pcrd_csv_path = 'C:/Users/PrasadNair/Desktop/Knee/data/PCR-211006-2218-SB6_384'
    #pcrd_fname = 'PCR-211006-2218-SB6_384'
    #save_plots_flag = 1
    
    save_plots_flag = int(save_plots_flag)
    if (save_plots_flag == 1):
        fldr = pcrd_csv_path + '/' + pcrd_fname + ' - plots'
        if (os.path.isdir(fldr)):
            shutil.rmtree(fldr)
        os.mkdir(fldr)
                
    with open(pcrd_csv_path + '/' + pcrd_fname + ' - Knee Value Results.csv', 'w') as f:
        heading = 'Well,Target,Cq,Plate\n'  
        f.write(heading)
        
    for row_idx in range(0,len(row)): 
        for col_idx in range(0,len(col)):   
            for target_idx in range(0,len(target_names)): 
                knee = 'NaN'
                knee = compute_knee(row_idx, col_idx, target_idx, pcrd_csv_path, pcrd_fname, save_plots_flag)
                #print(knee)
                with open(pcrd_csv_path + '/' + pcrd_fname + ' - Knee Value Results.csv', 'a') as f:
                    col1 = col[col_idx]
                    if len(col1) == 1: col1 = '0' + col1
                    line = row[row_idx] + col1 + ',' + target_names[target_idx] + ',' + str(knee) + ',' + pcrd_fname[0:19] + '\n' 
                    #print(line)
                    f.write(line)
                    print(pcrd_fname + ':  ' + line)


if __name__ == "__main__":
    main(*sys.argv[1:])
